package es.sacyl.ccow;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import es.sacyl.ccow.ui.NonceUI;

@Theme("mytheme")
@SuppressWarnings("serial")
@Push
@PreserveOnRefresh
public class CCOWUI extends UI
{

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        setContent(layout);
        
        // Añadir la UI
        layout.addComponent(new NonceUI());
        
    }

}
