package es.sacyl.ccow.bridge;

import org.json.JSONException;
import org.json.JSONObject;

import es.sacyl.ccow.common.Constants;
import es.sacyl.ccow.common.exceptions.CCOWContextException;

/**
 * 
 * @author guzman
 * 
 * Esta es una de las clases más importantes, aquí se realizará el acceso a los sistemas externos,
 * así como la sincronización entre diferentes sistemas.
 * Aquí se definirá si CCOW funciona como maestro o ha de sincronizarse con otros servicios.
 *
 */
public class DummyContextBridge {

	/**
	 * Este método recupera la información, está en manos del desarrollador guardarla en el árbol o consultar cada vez.
	 * 
	 * @param usuario objeto del usuario actualmente almacenado
	 * @param parameter parámetro que se busca
	 * @return valor del parámetro buscado
	 * @throws CCOWContextException
	 */
	public String getParameter(JSONObject usuario, String parameter) throws CCOWContextException {

		String value = null;

		if (usuario != null) {
			try {
				
				/*
				 * Aquí debería ir la lógica para recuperar la información que le falte al árbol
				 */
				
				if (!usuario.has("app1")) {
					usuario.append("app1", new JSONObject().append("user", "guz"));
				}
				if (!usuario.has("app2")) {
					usuario.append("app2", new JSONObject().append("usuario", "guzman"));
				}
				
				if (Constants.USER_ID.equals(parameter)) {
					return usuario.getString(Constants.USER_ID);
				} else if (Constants.NOMBRE.equals(parameter)) {
					return usuario.getString(Constants.NOMBRE);
				} else if (Constants.APELLIDOS.equals(parameter)) {
					return usuario.getString(Constants.APELLIDOS);
				}
				
				// Aquí devolvemos un objeto JSON entero, y se reprocesará en el cliente
				if (Constants.TODO.equals(parameter)) {
					return usuario.toString();
				}
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return value;

	}
	


	/**
	 * Este método actualiza la información, está en manos del desarrollador guardarla en el árbol o sincronizarla con otros sistemas.
	 * 
	 * @param usuario objeto del usuario actualmente almacenado
	 * @param parameter parámetro que se busca
	 * @param value nuevo valor a asignar
	 * @throws CCOWContextException
	 */
	public void setParameter(JSONObject usuario, String parameter, String value) throws CCOWContextException {
		
		if (usuario != null) {
			try {
				usuario.put(parameter, value);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
	}

}
