package es.sacyl.ccow.bridge;

import org.json.JSONException;
import org.json.JSONObject;

import es.sacyl.ccow.common.Constants;
import es.sacyl.ccow.common.exceptions.CCOWLoginException;

/**
 * 
 * @author guzman
 * 
 * En esta clase es donde se realizará el enlace con el servicio de autentificación.
 *
 */
public class DummyLoginBridge {

	public JSONObject login(String usuario, String password) throws CCOWLoginException {

		// TODO
		// Aquí debería ir la validación.

		JSONObject object = new JSONObject();

		try {

			/*
			 * Esta sería la información de inicio, o la información común a todas las aplicaciones.
			 */
			
			object.append(Constants.USER_ID, usuario);
			object.append(Constants.NOMBRE, "Guzman");
			object.append(Constants.APELLIDOS, "Arce");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return object;

	}

}
