package es.sacyl.ccow.common;

import java.util.HashMap;

public class Constants {

	/* Estas con las keys de los atributos para JSON */
	
	// Este es el atributo principal, que ha de ser único para todo el sistema
	public static final String USER_ID = "USER_ID";
	
	// Valores
	public static final String NOMBRE = "NOMBRE";
	public static final String APELLIDOS = "APELLIDOS";
	public static final String TODO = "TODO";

	// Aplicaciones admitidas, con su clave
	public static HashMap<String, String> authenticatedApplications = new HashMap<String, String>();
	static {
		// TODO esto hay que cargarlo de fichero o BD
		authenticatedApplications.put("APP1", "mipassword");
	}
	
}
