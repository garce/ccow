package es.sacyl.ccow.common;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;

import org.jboss.resteasy.util.Hex;

/**
 * 
 * @author guzman
 * 
 * Esta clase genera los NONCE.
 * 
 * Estos objetos están definidos por un código único y un tiempo de creación.
 * Aquí también se define el tiempo de vida que tendrán los objetos.
 *
 */
public class Nonce {
	
	private static final long ALIVE = 300000; // 5 min = 300000, 10 seg = 10000
	private static final Random RND = new SecureRandom();
	
	private byte[] nonce;

	private long creationDate = System.currentTimeMillis();
	
	/**
	 * Creates a new nonce with the given byte array.
	 * 
	 * @param nonce the byte array.
	 */
	public Nonce(byte[] nonce) {
		this.nonce = nonce;
	}
	
	/**
	 * Returns the nonce byte array.
	 * 
	 * @return the byte array.
	 */
	public byte[] getBytes() {
		return nonce;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object o) {
		Nonce n = (Nonce) o;
		
		return Arrays.equals(n.nonce, nonce);
	}
	
	@Override
	public String toString() {
		return Hex.encodeHex(nonce);
	}
	
	public boolean isValid() {
		return (System.currentTimeMillis() - creationDate ) < ALIVE;
	}
	
	/**
	 * Generates a new random Nonce.
	 * <p>
	 * This method does not guarantee that multiple invocations will produce a different
	 * nonce, as the byte generation is provided by a SecureRandom instance.
	 * 
	 * @return the generated nonce.
	 * @see java.security.SecureRandom
	 */
	public static Nonce nextNonce() {
		
		byte[] bytes = new byte[16];
		RND.nextBytes(bytes);

		final Nonce nonce = new Nonce(bytes);
		
		return nonce;
	}
	
}
