package es.sacyl.ccow.common;

/**
 * 
 * @author guzman
 * 
 * Estos son los identificadores únicos por usuario/aplicación.
 * 
 */
public class Token {
	
	public static final String COOKIE = "COOKIE";

	private String value;
	private String app;
	
	public Token(String value, String app) {
		this.value = value;
		this.app = app;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getApp() {
		return app;
	}
	public void setApp(String app) {
		this.app = app;
	}
	
}
