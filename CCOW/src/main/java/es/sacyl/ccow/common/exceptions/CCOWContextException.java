package es.sacyl.ccow.common.exceptions;

/**
 * 
 * @author guzman
 *
 * Excepción asociada a un error de contexto.
 *
 */
public class CCOWContextException extends Exception {

	public CCOWContextException(String msg) {
		super(msg);
	}

	public CCOWContextException(Throwable cause) {
		super(cause);
	}

	public CCOWContextException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
