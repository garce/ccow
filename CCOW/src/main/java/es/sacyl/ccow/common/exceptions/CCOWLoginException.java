package es.sacyl.ccow.common.exceptions;

/**
 * 
 * @author guzman
 *
 * Excepción asociada a un error de login.
 *
 */
public class CCOWLoginException extends Exception {

	public CCOWLoginException(String msg) {
		super(msg);
	}

	public CCOWLoginException(Throwable cause) {
		super(cause);
	}

	public CCOWLoginException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
