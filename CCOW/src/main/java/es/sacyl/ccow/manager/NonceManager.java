package es.sacyl.ccow.manager;

import java.util.HashSet;
import java.util.Hashtable;

import com.vaadin.external.org.slf4j.Logger;
import com.vaadin.external.org.slf4j.LoggerFactory;

import es.sacyl.ccow.common.Nonce;


/**
 * @author guzman
 * 
 * Esta clase es el manager de los Nonce, aquí es donde se crean y se gestionan los nonce.
 * 
 * Los dos métodos principales son:
 * 	- nextNonce: devuelve un nuevo nonce.
 *  - consume: procesa un nonce, devolviendo si era válido o no.
 * 
 */
public class NonceManager {
	
	final static Logger logger = LoggerFactory.getLogger(NonceManager.class);

	// Constantes para la limpieza
	private static final long MAX_SIZE_WITHOUT_CLEANING = 1000000;
	private static final long TIME_BETWEEN_CLEANING = 600000; // 10 min
	private static boolean cleaning = false;
	
	// Fecha de última limpieza
	private static long last_clean = 0;
	
	// Almacen de nonce
	public static Hashtable<String, Nonce> tree = new Hashtable<String, Nonce>();

	/**
	 * Generar un nuevo Nonce y añadirlo al repositorio.
	 */
	public static synchronized Nonce nextNonce() {
		
		// Inicializar
		Nonce nonce = null;
		String key;
		
		boolean ok = false;
		while (!ok) {
			// Generar
			nonce = Nonce.nextNonce();
			key = nonce.toString();
			// Comprobamos que no exista ya
			if (!tree.containsKey(key)) {
				// Guardar
				tree.put(key, nonce);
				ok = true;
			}
		}
		
		// Comprobar limpieza
		if (tree.size() > MAX_SIZE_WITHOUT_CLEANING) {
			if ((System.currentTimeMillis() - last_clean) > TIME_BETWEEN_CLEANING) {
				if (!cleaning) {
					// Marcar que estamos limpiando
					cleaning = true;
					// Lanzar el hilo!
					(new HiloLimpieza()).start();
				}
			}
		}
		
		logger.debug("Generado nonce :: " + nonce);
		
		// Devolver
		return nonce;
		
	}
	
	/**
	 * Eliminar TODOS los nonce
	 */
	public static synchronized void clear() {
		tree.clear();
	}
	
	/**
	 * Al consumirlo, comprobamos si existe y es válido, y después de esto elimina el objeto
	 * @param key clave que hay que comprobar
	 * @return devuelve si el objeto era válido
	 */
	public static synchronized boolean consume(String key) {
		if (key != null) {
			Nonce nonce = tree.get(key);
			if (nonce != null) {
				// Eliminamos el objeto
				tree.remove(key);
				// Log
				logger.debug("Consumido nonce :: " + nonce);
				// Devolvemos si es válido
				return nonce.isValid();
			}
		}
		return false;
	}
	
	/**
	 * Devolver el tamaño (para la UI)
	 * @return
	 */
	public static int getSize() {
		return tree.size();
	}
	
	/**
	 * Limpiar externamente (forzado desde UI)
	 */
	public static synchronized void clean() {
		// Marcar que estamos limpiando
		cleaning = true;
		// Lanzar el hilo!
		(new HiloLimpieza()).start();
	}
	
	
	
	static class HiloLimpieza extends Thread {
		public void run() {
			// Limpiar
			removeAllInvalid();
			// Marcar fecha de última limpieza
			last_clean = System.currentTimeMillis();
			// Marcar el boolean para limpieza
			cleaning = false;
		}
	}
	
	/**
	 * Eliminar los inválidos
	 */
	private static synchronized void removeAllInvalid() {
		// Aquí almacenamos los inválidos
		HashSet<String> invalidos = new HashSet<String>();
		// Comprobar
		for (Nonce n : tree.values()) {
			if (!n.isValid()) {
				invalidos.add(n.toString());
			}
		}
		// Eliminar
		for (String s : invalidos) {
			tree.remove(s);
		}
	}
	
}
