package es.sacyl.ccow.manager;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

import org.jboss.resteasy.util.Hex;
import org.json.JSONObject;

import es.sacyl.ccow.bridge.DummyContextBridge;
import es.sacyl.ccow.bridge.DummyLoginBridge;
import es.sacyl.ccow.common.Token;
import es.sacyl.ccow.common.exceptions.CCOWContextException;
import es.sacyl.ccow.common.exceptions.CCOWLoginException;

/**
 * 
 * @author guzman
 * 
 * Esta es la clase principal por donde entran los métodos REST, y donde están expuestas las funcionalidades.
 * 
 * REST/UI ==> UserManager ==> Lógica
 * 
 * Además, aquí se gestiona la lógica de autentificación de usuarios, se crean y asignan los token, y se supervisa
 * la seguridad de la aplicación.
 *
 */
public class UserManager {
	
	private UserManager() {}
	private static UserManager instance = null;
	public static UserManager getInstance() {
		if (instance == null) instance = new UserManager();
		return instance;
	}
	
	private DummyLoginBridge loginBridge = new DummyLoginBridge();
	private DummyContextBridge contextBridge = new DummyContextBridge();

	// Aquí guardamos los usuarios
	// <idUsuario, JSONObject>
	private Hashtable<String, JSONObject> usuarios = new Hashtable<String, JSONObject>();

	// Aquí guardamos los tokens que tiene cada usuario
	// <idUsuario, <valueToken, token>>
	private Hashtable<String, List<Token>> usuariosTodosToken = new Hashtable<String, List<Token>>();
	
	// Aquí guardamos un enlace entre hash y usuario
	// <valueToken, idUsuario>
	private HashMap<String, String> tokenUsuario = new HashMap<String, String>();
	
	/**
	 * Realiza el login y devuelve el token asociado a la aplicación
	 */
	public String login(String app, String user, String pass) {
		
		try {
			JSONObject usuarioContenedor = loginBridge.login(user, pass);
			
			if (usuarioContenedor != null) {
				// El login es correcto
				
				// Comprobamos si existe ya el usuario
				if (usuarios.containsKey(user)) {
					// Si entra aquí es que se ha logueado en alguna aplicación
					
					for (Token t : usuariosTodosToken.get(user)) {
						if (t.getApp().equals(app)) {
							// Si entra aquí es que estamos intentando hacer login con la misma aplicación (tope chungo)
							return t.getValue();
						}
					}
					// Si llegamos aquí es que no hay token para esa app en concreto
					
				} else {
					// El usuario no existía
					usuarios.put(user, usuarioContenedor);
				}
				
				// Si pasamos hasta aquí es que el usuario no estaba logueado
				
				// Generamos token
				Token token = new Token(getToken(), app);
				
				// Añadimos a los hash pertinentes
				tokenUsuario.put(token.getValue(), user);
				List<Token> tokens = usuariosTodosToken.get(user);
				if (tokens == null) {
					// Crear el nuevo
					tokens = new ArrayList<Token>();
					// Añadirlo al hash
					usuariosTodosToken.put(user, tokens);
				}
				tokens.add(token);
				
				// Devolvemos el token asignado al usuario y la aplicación
				return token.getValue();
				
			}
			
		} catch (CCOWLoginException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Devuelve el token para asociar a la cookie, lógicamente sólo puede haber UNO por usuario
	 * @param token token identificador del usuario
	 * @return token para cookie
	 */
	public String tokenCookie(String app, String token) {
		
		// Recuperar el id del usuario
		String userid = tokenUsuario.get(token);
		if (userid == null) {
			// El token no existe
			return null;
		}
		
		// Recuperar los tokens del usuario
		List<Token> tokensUsuario = usuariosTodosToken.get(userid);
		if (tokensUsuario != null) {	// Tiene que ser null porque al menos tiene que tener el de login
			// Comprobamos que no tenga el token
			for (Token t : tokensUsuario) {
				if (Token.COOKIE.equals(t.getApp())) {
					return null;
				}
			}
			// Creamos el nuevo
			Token t = new Token(getToken(), Token.COOKIE);
			// Añadimos a los hash
			tokenUsuario.put(t.getValue(), userid);
			tokensUsuario.add(t);
			return t.getValue();
		}
		
		return null;
		
	}
	
	/**
	 * Devuelve el token para asociar a la app que digamos, lógicamente sólo puede haber UNO por usuario y app (si ya existiera lo devuelve)
	 * @param token token identificador del usuario
	 * @param appDestino token identificador de la app destino
	 * @return token para cookie
	 */
	public String tokenApp(String app, String token, String appDestino) {
		
		if (appDestino == null || "".equals(appDestino)) {
			return null;
		}
		
		// Recuperar el id del usuario
		String userid = tokenUsuario.get(token);
		if (userid == null) {
			// El token no existe
			return null;
		}
		
		// Recuperar los tokens del usuario
		List<Token> tokensUsuario = usuariosTodosToken.get(userid);
		if (tokensUsuario != null) {	// Tiene que ser null porque al menos tiene que tener el de login
			// Comprobamos que no tenga el token
			for (Token t : tokensUsuario) {
				if (appDestino.equals(t.getApp())) {
					return t.getValue();
				}
			}
			// Creamos el nuevo
			Token t = new Token(getToken(), appDestino);
			// Añadimos a los hash
			tokenUsuario.put(t.getValue(), userid);
			tokensUsuario.add(t);
			return t.getValue();
		}
		
		return null;
		
	}
	
	public boolean perteneceTokenApp(String app, String token) {
		String userid = tokenUsuario.get(token);
		if (userid != null) {
			List<Token> tokensUsuario = usuariosTodosToken.get(userid);
			if (tokensUsuario != null) {
				// Buscar el token
				Token t = null;
				for (Token t0 : tokensUsuario) {
					if (t0.getValue().equals(token)) t = t0;
				}
				// Devolver el token
				if (t != null) {
					return t.getApp().equals(app);
				}
			}
		}
		return false;
	}
	
	/**
	 * Devolvemos el UsuarioContenedor de un token
	 * @param token
	 * @return
	 */
	public JSONObject getUsuario(String token) {
		// Recuperamos el userid en base al token
		String userid = tokenUsuario.get(token);
		if (userid != null && !"".equals(userid)) {
			return usuarios.get(userid);
		}
		return null;
	}
	
	/**
	 * Devuelve un parámetro del contexto
	 * 
	 * @param token
	 * @param parametro
	 * @return
	 */
	public String getContexto(String token, String parametro) {
		try {
			return contextBridge.getParameter(getUsuario(token), parametro);
		} catch (CCOWContextException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error";
		}
	}
	
	/**
	 * Actualiza un parámetro del contexto
	 * 
	 * @param token
	 * @param parametro
	 * @return
	 */
	public boolean setContexto(String token, String parametro, String value) {
		try {
			contextBridge.setParameter(getUsuario(token), parametro, value);
		} catch (CCOWContextException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/*
	 * Métodos para generar un token que no esté en uso
	 */
	private final Random RND = new SecureRandom();
	public String getToken() {
		String token = null;
		boolean ok = false;
		while (!ok) {
			byte[] bytes = new byte[16];
			RND.nextBytes(bytes);
			token = Hex.encodeHex(bytes);
			if (!tokenUsuario.containsKey(token)) {
				ok = true;
			}
		}
		return token;
	}
	
}
