package es.sacyl.ccow.rest;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import es.sacyl.ccow.common.Constants;
import es.sacyl.ccow.manager.NonceManager;
import es.sacyl.ccow.manager.UserManager;

/**
 * 
 * @author guzman
 * 
 * Esta clase gestiona la comunicación segura a través del sistema NONCE.
 *
 */
public class AuthenticatedCommunication {

	/**
	 *  Instancia del userManager, visible a las clases que implementan REST.
	 */
	protected UserManager userManager = UserManager.getInstance();
	
	/**
	 * Valida la comunicación
	 * @param app
	 * @param nonce
	 * @param hash
	 * @return
	 */
	protected boolean authenticate(String app, String nonce, String hash) {
		
		// Lo primero comprobar el nonce
		if (NonceManager.consume(nonce)) {
			// Nonce válido
			
			// Recuperar password de app
			String claveApp = Constants.authenticatedApplications.get(app);
			if (claveApp != null) {
				
				try {
					MessageDigest md = MessageDigest.getInstance("MD5");
					byte[] bytesHash = (nonce + claveApp).getBytes();
					String miHash = new BigInteger(1, md.digest(bytesHash)).toString(16);
					
					// Devolvemos si el hash coincide
					return miHash.equals(hash);
					
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}
		
		return false;
	}
	
	/**
	 * Validamos que el token que nos llega esté asociado a la aplicación que nos llega
	 * La validación de que esa app es la que dice ser se hace a través del nonce
	 * @param app
	 * @param token
	 * @return
	 */
	protected boolean authorizeToken(String app, String token) {
		return userManager.perteneceTokenApp(app, token);
	}
	
}
