package es.sacyl.ccow.rest;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

/**
 * 
 * @author guzman
 * 
 * REST.
 * Servicio de autentificación.
 *
 */

@Path("/aut")
public class AuthenticationService extends AuthenticatedCommunication {

//	/**
//	 * Recuperar un nuevo token en base a uno recibido, que puede ser:
//	 * - Recibido desde otra aplicación
//	 * - Recibido desde cookie
//	 */
//	@GET
//    @Path("/token/{token}")
//    public String getNewToken(@PathParam("token") String key)
//    {
//        return NonceManager.nextNonce().toString();
//    }

	/**
	 * Realizar el login, y devolver el token asociado
	 * @param user
	 * @param pass
	 * @return token
	 */
	@POST
	@Path("/login")
	public String login(
			@FormParam("app") String app,
			@FormParam("nonce") String nonce,
			@FormParam("hash") String hash,
			@FormParam("user") String user,
			@FormParam("pass") String pass) {
		
		// Validar la comunicación
		if (authenticate(app, nonce, hash)) {
			return userManager.login(app, user, pass);
		}
		return null;
		
	}

	/**
	 * Realizar el login a través del token de cookie, y devolver el token asociado
	 * @param user
	 * @param pass
	 * @return token
	 */
	@POST
	@Path("/token/cookie")
	public String tokenCookie(
			@FormParam("app") String app,
			@FormParam("nonce") String nonce,
			@FormParam("hash") String hash,
			@FormParam("token") String token) {
		
		// Validar la comunicación
		if (authenticate(app, nonce, hash) && authorizeToken(app, token)) {
			return userManager.tokenCookie(app, token);
		}
		return null;
		
	}

	/**
	 * Realizar el login a través del token de otra app, y devolver el token asociado
	 * @param user
	 * @param pass
	 * @return token
	 */
	@POST
	@Path("/token/app")
	public String tokenApp(
			@FormParam("app") String app,
			@FormParam("nonce") String nonce,
			@FormParam("hash") String hash,
			@FormParam("token") String token,
			@FormParam("appDestino") String appDestino) {
		
		// Validar la comunicación
		if (authenticate(app, nonce, hash) && authorizeToken(app, token)) {
			return userManager.tokenApp(app, token, appDestino);
		}
		return null;
		
	}
	
	/**
	 * Logout del usuario asociado al token
	 * @param token
	 * @return
	 */
	@PUT
	@Path("/logout")
	public boolean logout(
			@FormParam("app") String app,
			@FormParam("nonce") String nonce,
			@FormParam("hash") String hash,
			@FormParam("token") String token) {
		
		// Validar la comunicación
		if (authenticate(app, nonce, hash) && authorizeToken(app, token)) {
			
		}
		
		return true;
		
	}
	
}
