package es.sacyl.ccow.rest;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 * 
 * @author guzman
 * 
 * REST.
 * Gestión del contexto.
 *
 */

@Path("/contexto")
public class ContextService extends AuthenticatedCommunication {

	@GET
	@Path("/{parametro}")
	public String parametro(
			@HeaderParam("app") String app,
			@HeaderParam("nonce") String nonce,
			@HeaderParam("hash") String hash,
			@HeaderParam("token") String token,
			@PathParam("parametro") String parametro) {
		
		// Validar la comunicación
		if (authenticate(app, nonce, hash) && authorizeToken(app, token)) {
			return userManager.getContexto(token, parametro);
		}
		return null;
		
	}

	@POST
	@Path("/{parametro}")
	public String parametro(
			@FormParam("app") String app,
			@FormParam("nonce") String nonce,
			@FormParam("hash") String hash,
			@FormParam("token") String token,
			@FormParam("value") String value,
			@PathParam("parametro") String parametro) {
		
		// Validar la comunicación
		if (authenticate(app, nonce, hash) && authorizeToken(app, token)) {
				if (userManager.setContexto(token, parametro, value)) {
					return "ok";
				}
		}
		return "ko";
		
	}
	
}
