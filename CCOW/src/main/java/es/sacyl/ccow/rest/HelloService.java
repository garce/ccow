package es.sacyl.ccow.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * 
 * @author guzman
 * 
 * REST/HTML.
 * Página por defecto.
 *
 */

@Path("/")
public class HelloService {
    
	@GET
    public String hello()
    {
        return "No deberias estar aqui";
    }
}
