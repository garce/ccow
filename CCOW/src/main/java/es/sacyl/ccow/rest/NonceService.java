package es.sacyl.ccow.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import es.sacyl.ccow.manager.NonceManager;

/**
 * 
 * @author guzman
 * 
 * REST.
 * Servicio de petición de NONCE.
 *
 */

@Path("/nonce")
public class NonceService {
    
	@GET
    public String getNonce()
    {
        return NonceManager.nextNonce().toString();
    }

//	/**
//	 * Ojo con este método, hay que comentarlo para producción, sólo se debería usar para comprobaciones.
//	 * @param key
//	 * @return
//	 */
//	@GET
//    @Path("/valido/{key}")
//	public boolean isValid(@PathParam("key") String key) {
//		Nonce nonce = NonceManager.getNonce(key);
//		if (nonce != null) {
//			return nonce.isValid();
//		}
//		return false;
//	}
	
}
