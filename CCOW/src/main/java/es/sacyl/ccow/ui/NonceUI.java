package es.sacyl.ccow.ui;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

import es.sacyl.ccow.manager.NonceManager;

/**
 * 
 * @author guzman
 * 
 * Clase para la UI que muestra el número de nonce activos, y permite limpiar los no utilizados,
 * aunque por defecto se limpian periódicamente.
 *
 */
public class NonceUI extends CustomComponent {

	private HorizontalLayout mainLayout;

	private Label size = new Label();
	private Button clear = new Button("Limpiar claves inválidas");
	private Button removeAll = new Button("Invalidar todas las claves");

	private boolean actualiza = true;

	public NonceUI() {
		mainLayout = new HorizontalLayout();
		setCompositionRoot(mainLayout);

		mainLayout.setSpacing(true);
		mainLayout.addComponent(size);
		mainLayout.addComponent(clear);
		mainLayout.addComponent(removeAll);
		// mainLayout.addComponent(last);
		
		mainLayout.setComponentAlignment(size, Alignment.BOTTOM_LEFT);
		clear.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				NonceManager.clean();
			}
		});
		removeAll.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				NonceManager.clear();
			}
		});

		new ActualizaThread().start();

	}

	class ActualizaThread extends Thread {
		@Override
		public void run() {

			while (actualiza) {

				// Init done, update the UI after doing locking
				getUI().getCurrent().access(new Runnable() {
					@Override
					public void run() {
						size.setValue("Número de nonce almacenados : " + NonceManager.getSize());
					}
				});

				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
				}

			}
		}
	}

	@Override
	public void detach() {
		actualiza = false;
		super.detach();
	}

}
