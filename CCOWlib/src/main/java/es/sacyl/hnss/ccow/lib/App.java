package es.sacyl.hnss.ccow.lib;

import org.json.JSONObject;



/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	
    	try {
    		
			CCOWOperator.getInstance().configure(new Configuration("http://localhost:8080/CCOW/", "APP1", "mipassword"));
			
			String token = CCOWOperator.getInstance().login("user", "password");
			System.out.println("token = " + token);
			
//			String tokenCookie = Operator.getInstance().tokenCookie(token);
//			System.out.println("tokenCookie = " + tokenCookie);
			
			String tokenApp = CCOWOperator.getInstance().tokenApp(token, "APP2");
			System.out.println("tokenApp = " + tokenApp);
			String tokenApp2 = CCOWOperator.getInstance().tokenApp(token, "APP2");
			System.out.println("tokenApp = " + tokenApp2);

			// Recuperamos parámetros
			String userid = CCOWOperator.getInstance().getParametro(token, "USER_ID");
			System.out.println();
			System.out.println("userid = " + userid);
			String nombre = CCOWOperator.getInstance().getParametro(token, "NOMBRE");
			System.out.println("nombre = " + nombre);
			String apellidos = CCOWOperator.getInstance().getParametro(token, "APELLIDOS");
			System.out.println("apellidos = " + apellidos);
			
			// Recuperamos toda la información
			String todo = CCOWOperator.getInstance().getParametro(token, "TODO");
			System.out.println();
			System.out.println("todo = " + todo);
			System.out.println("todo JSONObject = " + new JSONObject(todo));
			System.out.println("JSONObject (nombre) = " + new JSONObject(todo).get("NOMBRE"));
			
			// Actualizamos un parametro
			CCOWOperator.getInstance().setParametro(token, "APELLIDOS", "Saez");
			
			// Recuperamos toda la información (otra vez)
			todo = CCOWOperator.getInstance().getParametro(token, "TODO");
			System.out.println();
			System.out.println("todo = " + todo);
			System.out.println("todo JSONObject = " + new JSONObject(todo));
			System.out.println("JSONObject (nombre) = " + new JSONObject(todo).get("NOMBRE"));
			
		} catch (CCOWException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}
