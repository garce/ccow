package es.sacyl.hnss.ccow.lib;

public class CCOWException extends Exception {

	public CCOWException(String msg) {
		super(msg);
	}

	public CCOWException(Throwable cause) {
		super(cause);
	}

	public CCOWException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
