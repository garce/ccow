package es.sacyl.hnss.ccow.lib;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class CCOWOperator {
	
	// Singleton
	private static CCOWOperator instance = null;
	private CCOWOperator() {
	}
	public synchronized static CCOWOperator getInstance() {
		if (instance == null) {
			instance = new CCOWOperator();
		}
		return instance;
	}

	// Configuración
	private Configuration configuration = null;
	public void configure(Configuration configuration) {
		this.configuration = configuration;
	}
	
	
	// Inicio de métodos
	
	/****************************************************** LOGIN ******************************************************/
	
	/**
	 * Solicita un nonce al servidor.
	 * @return nonce
	 */
	private String getNonce() throws CCOWException {
		
		try {
			URL url = new URL(configuration.getCCOW_URL() + "nonce");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
 
			if (conn.getResponseCode() != 200) {
				throw new CCOWException("No se ha podido recuperar NONCE");
			}
 
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			StringBuffer buffer = new StringBuffer();
			String output;
			while ((output = br.readLine()) != null) {
				buffer.append(output);
			}
			conn.disconnect();
			return buffer.toString();
		} catch (MalformedURLException e) {
			return null;
		} catch (ProtocolException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}
	
	/**
	 * Realiza el hash del nonce (sumando la contraseña de la aplicación)
	 * @param nonce el nonce a utilizar
	 * @return el hash creado (nonce + passApp)
	 */
	private String getHash(String nonce) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] bytesHash = (nonce + configuration.getApplicationPassword()).getBytes();
			return new BigInteger(1, md.digest(bytesHash)).toString(16);
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}
	
	/**
	 * Realiza login contra el servidor CCOW
	 * @param user usuario de conexión
	 * @param pass password en texto plano (necesario así por LDAP)
	 * @return token o null, en caso de que haya algún error lanza una excepción
	 * @throws CCOWException
	 */
	public String login(String user, String pass) throws CCOWException {
		
		if (configuration == null) throw new CCOWException("No has configurado la conexión con CCOW");
		
		try {
			
			// Generar los parámetros
			String nonce = getNonce();
			String hash = getHash(nonce);
			
			// Abrir la conexión
			URL url = new URL(configuration.getCCOW_URL() + "aut/login");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			
			// Añadir los parámetros
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("app", configuration.getApplicationName()));
			nvps.add(new BasicNameValuePair("nonce", nonce));
			nvps.add(new BasicNameValuePair("hash", hash));
			nvps.add(new BasicNameValuePair("user", user));
			nvps.add(new BasicNameValuePair("pass", pass));
			
			OutputStream os = conn.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
			writer.write(getQuery(nvps));
			writer.close();
			os.close();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				// Lanzamos excepción
				throw new CCOWException("Login erróneo para usuario " + user);
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			StringBuffer output = new StringBuffer();
			String line;
			while ((line = br.readLine()) != null) {
				output.append(line);
			}

			conn.disconnect();
			
			return output.toString();
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
		
	}
	
	/**
	 * Genera un token para asignar a la cookie y hacerlo disponible para el resto de aplicaciones
	 * @param token token del usuario a partir del cual generamos el token de cookie
	 * @return token asociado al usuario, para guardar en la cookie
	 * @throws CCOWException
	 */
	public String tokenCookie(String token) throws CCOWException {
		
		if (configuration == null) throw new CCOWException("No has configurado la conexión con CCOW");
		
		try {
			
			// Generar los parámetros
			String nonce = getNonce();
			String hash = getHash(nonce);
			
			// Abrir la conexión
			URL url = new URL(configuration.getCCOW_URL() + "aut/token/cookie");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			
			// Añadir los parámetros
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("app", configuration.getApplicationName()));
			nvps.add(new BasicNameValuePair("nonce", nonce));
			nvps.add(new BasicNameValuePair("hash", hash));
			nvps.add(new BasicNameValuePair("token", token));
			
			OutputStream os = conn.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
			writer.write(getQuery(nvps));
			writer.close();
			os.close();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				// Lanzamos excepción
				throw new CCOWException("No se ha podido recuperar token de cookie para token "  + token);
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			StringBuffer output = new StringBuffer();
			String line;
			while ((line = br.readLine()) != null) {
				output.append(line);
			}

			conn.disconnect();
			
			return output.toString();
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
		
	}
	
	/**
	 * Genera un token para asignar a la cookie y hacerlo disponible para el resto de aplicaciones
	 * @param token token del usuario a partir del cual generamos el token de cookie
	 * @param appDestino identificador de la app a la que va a estar asociado el token
	 * @return token asociado al usuario, para la nueva aplicación
	 * @throws CCOWException
	 */
	public String tokenApp(String token, String appDestino) throws CCOWException {
		
		if (configuration == null) throw new CCOWException("No has configurado la conexión con CCOW");
		
		try {
			
			// Generar los parámetros
			String nonce = getNonce();
			String hash = getHash(nonce);
			
			// Abrir la conexión
			URL url = new URL(configuration.getCCOW_URL() + "aut/token/app");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			
			// Añadir los parámetros
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("app", configuration.getApplicationName()));
			nvps.add(new BasicNameValuePair("nonce", nonce));
			nvps.add(new BasicNameValuePair("hash", hash));
			nvps.add(new BasicNameValuePair("token", token));
			nvps.add(new BasicNameValuePair("appDestino", appDestino));
			
			OutputStream os = conn.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
			writer.write(getQuery(nvps));
			writer.close();
			os.close();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				// Lanzamos excepción
				throw new CCOWException("No se ha podido recuperar token de cookie para token "  + token);
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			StringBuffer output = new StringBuffer();
			String line;
			while ((line = br.readLine()) != null) {
				output.append(line);
			}

			conn.disconnect();
			
			return output.toString();
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
		
	}
	

	/****************************************************** CONTEXTO ******************************************************/
	

	public String getParametro(String token, String parametro) throws CCOWException {
		
		try {
			
			// Generar los parámetros
			String nonce = getNonce();
			String hash = getHash(nonce);
			
			URL url = new URL(configuration.getCCOW_URL() + "contexto/" + parametro);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.addRequestProperty("app", configuration.getApplicationName());
			conn.addRequestProperty("nonce", nonce);
			conn.addRequestProperty("hash", hash);
			conn.addRequestProperty("token", token);
 
			if (conn.getResponseCode() != 200) {
				// Lanzamos excepción
//				throw new CCOWException("No se ha podido recuperar el parámetro " + parametro);
			}
 
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			StringBuffer buffer = new StringBuffer();
			String output;
			while ((output = br.readLine()) != null) {
				buffer.append(output);
			}
			conn.disconnect();
			return buffer.toString();
		} catch (MalformedURLException e) {
			return null;
		} catch (ProtocolException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}
	

	public String setParametro(String token, String parametro, String value) throws CCOWException {
		
		try {
			
			// Generar los parámetros
			String nonce = getNonce();
			String hash = getHash(nonce);
			
			// Abrir la conexión
			URL url = new URL(configuration.getCCOW_URL() + "contexto/" + parametro);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			
			// Añadir los parámetros
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("app", configuration.getApplicationName()));
			nvps.add(new BasicNameValuePair("nonce", nonce));
			nvps.add(new BasicNameValuePair("hash", hash));
			nvps.add(new BasicNameValuePair("token", token));
			nvps.add(new BasicNameValuePair("value", value));
			
			OutputStream os = conn.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
			writer.write(getQuery(nvps));
			writer.close();
			os.close();

			if (conn.getResponseCode() != 200) {
				// Lanzamos excepción
//				throw new CCOWException("No se ha podido recuperar el parámetro " + parametro);
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			StringBuffer output = new StringBuffer();
			String line;
			while ((line = br.readLine()) != null) {
				output.append(line);
			}

			conn.disconnect();
			
			return output.toString();
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
		
	}
	
	
	
	/**
	 * Método utilizado para realizar los envíos por POST
	 */
	private static String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
		StringBuilder result = new StringBuilder();
		boolean first = true;

		for (NameValuePair pair : params) {
			if (first)
				first = false;
			else
				result.append("&");

			result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
		}

		return result.toString();
	}
	
}
