package es.sacyl.hnss.ccow.lib;

public class Configuration {

	private String CCOW_URL;
	private String applicationName;
	private String applicationPassword;
	
	public Configuration(String CCOW, String applicationName, String applicationPassword) {
		this.CCOW_URL = CCOW;
		this.applicationName = applicationName;
		this.applicationPassword = applicationPassword;
	}


	public String getCCOW_URL() {
		return CCOW_URL;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public String getApplicationPassword() {
		return applicationPassword;
	}
	
}
